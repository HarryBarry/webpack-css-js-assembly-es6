'use strict';

let webpack = require("webpack");
let ExtractTextPlugin = require("extract-text-webpack-plugin");
let UglifyJsPlugin = require('webpack/lib/optimize/UglifyJsPlugin');

let PROD = JSON.parse(process.env.NODE_ENV || '0');

module.exports = {
    context: __dirname + '/src',
    entry: {
        main: ['./app']
    },
    output: {
        path: __dirname + '/public',
        publicPath: '/',
        filename: PROD ? './production/js/we.min.js': './development/js/we.dev.build.js'
    },
    watch: true,
    watchOptions: {
        aggregateTimeout: 100
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                include: __dirname + '/src',
                exclude: /(node_modules|bower_components)/,
                loader: 'babel?presets[]=es2015'
            },
            {
                test: /\.html$/,
                loader: 'raw'
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('style', 'css!sass?resolve url')
            },
            {
                test: /\.(jpg|jpeg|gif|png)$/,
                exclude: /node_modules/,
                loader:'url-loader?limit=1024&name=images/[name].[hash].[ext]'
            },
            {
                test: /\.(woff|woff2|eot|ttf|svg)$/,
                exclude: /node_modules/,
                loader: 'url-loader?limit=1024&name=fonts/[name].[hash].[ext]'
            }
        ]
    },
    devServer: {
        host: 'localhost',
        port: 3000,
        contentBase: __dirname + '/src',
        historyApiFallback: true, // single page app
        hot: true // autoreload
    },
    plugins: PROD ? [
        new ExtractTextPlugin(
            "./production/css/we.min.css",
            { allChunks: true, disable: false } // dynamical loading styles
        ),
        new UglifyJsPlugin({
            sourceMap: false,
            mangle: false,
            minimize: true
        })
    ] : [
        new ExtractTextPlugin(
            "./development/css/we.dev.build.css",
            { allChunks: true, disable: true } // dynamical loading styles
        )
    ]
};