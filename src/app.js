import * as we from 'angular';

import 'angular-animate';
import 'angular-aria';
import 'angular-messages';
import 'angular-ui-router';

// Configs
import {RouteConfig} from './configs/routing/routes.config';

// Services
import {HeaderFactory} from './services/header/header.factory';

// App Directives
import {AppDirective}      from './components/app/app.directive';
import {HeaderDirective}   from './components/header/header.directive';

we.module('we', [ 'ngMessages', 'ngAnimate', 'ngAria', 'ui.router' ])
    .config(RouteConfig)

    .factory('HeaderFactory', HeaderFactory)

    .directive('weApp', AppDirective)
    .directive('weHeader', HeaderDirective)
;

we.element(document).ready(() => {
    we.bootstrap(document, ['we'])
});

console.log('%c WinterExpert Development Tools TEST ', "color: blue; font-size:20px;");