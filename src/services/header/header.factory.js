function HeaderFactory($http) {
    return {
        getLogo: () => {
            return './assets/images/samples/logo.svg';
        },

        getMenuItems: () => {
            return [
                {
                    'link': '',
                    'name': 'Home'
                },
                {
                    'link': 'category',
                    'name': 'Category'
                },
                {
                    'link': 'blog',
                    'name': 'Blog'
                }
            ];
        }
    }
}

HeaderFactory.$inject = ['$http'];
export {HeaderFactory};