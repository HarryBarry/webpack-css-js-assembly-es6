import {AppController} from './app.controller'

import './style/app.scss';

function AppDirective() {
    return {
        restrict: "A",
        controller: AppController,
        link: (scope, element, attrs) => {
            
        }
    }
}

AppDirective.$inject = [];

export {AppDirective}