import {HeaderController} from './header.controller'

import "./style/header.scss";

function HeaderDirective() {
    return {
        restrict: 'E',
        controller: HeaderController,
        template:
            `
            <header>
                <div class="itt_head"></div>
                <div class="itt_header-sticky">
                    <div class="row itt_container itt_head">
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-8">
                            <div class="itt_logo-wrap itt_flex itt_row itt_flex-1">
                                <div class="itt_logo">
                                    <a ui-sref="app.home" role="button">
                                        <img ng-src="{{ vm.logo }}" alt="winter expert">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="hidden-lg hidden-md hidden-sm col-xs-4 itt_bottom itt_center">
                            <div id="itt_sandwich" class="itt_sandwich" ng-click="showDropdown()">
                                <div class="itt_sw-topper" ng-class="{'sandwich-animate-top': isActive}"></div>
                                <div class="itt_sw-bottom" ng-class="{'sandwich-animate-bottom': isActive}"></div>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 itt_slide-toggle itt_navigation" ng-show="slideToggle">
                            <div class="row itt_right itt_h-100 itt_w-1_xs">
                                <nav role="navigation" class="itt_w-1_xs">
                                    <ul class="itt_nav-list">
                                        <!--<we-menu-item class="itt_nav-item" we-state="{{ menuItem.link }}" we-link-name="{{ menuItem.name }}" ng-repeat="menuItem in vm.menuItems"></we-menu-item>-->
                                        <li class="itt_nav-item">
                                            <div class="itt_stick-wrap">
                                                <div class="itt_stick itt_flex itt_flex-05 itt_center itt_h-100"></div>
                                            </div>
                                            <a ui-sref="app.home" class="itt_link" role="button">Home</a>
                                        </li>
                                        <li class="itt_nav-item">
                                            <div class="itt_stick-wrap">
                                                <div class="itt_stick itt_flex itt_flex-05 itt_center itt_h-100"></div>
                                            </div>
                                            <a ui-sref="app.category" class="itt_link" role="button">Categories</a>
                                        </li>
                                        <li class="itt_nav-item">
                                            <div class="itt_stick-wrap">
                                                <div class="itt_stick itt_flex itt_flex-05 itt_center itt_h-100"></div>
                                            </div>
                                            <a ui-sref="app.blog" class="itt_link" role="button">Blog</a>
                                        </li>
                                        <li class="itt_nav-item">
                                            <div class="itt_stick-wrap">
                                                <div class="itt_stick itt_flex itt_flex-05 itt_center itt_h-100"></div>
                                            </div>
                                            <a ui-sref="app.shop" class="itt_link" role="button">Shop</a>
                                        </li>
                                        <li class="itt_nav-item">
                                            <div class="itt_stick-wrap">
                                                <div class="itt_stick itt_flex itt_flex-05 itt_center itt_h-100"></div>
                                            </div>
                                            <a ui-sref="app.contacts" class="itt_link" role="button">Contacts</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 itt_center">
                            <div class="itt_search-wrap itt_flex itt_row">
                                <form action="#" method="get" class="itt_search-form">
                                    <input type="text" class="itt_search-field" placeholder="Search" name="s">
                                    <div id="itt_search-button" class="itt_search-button"><i class="fa fa-search" aria-hidden="true"></i></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            `,
        controllerAs: 'vm',
        link: (scope, element, attrs) => {

        }
    }
}

HeaderDirective.$inject = [];
export { HeaderDirective };