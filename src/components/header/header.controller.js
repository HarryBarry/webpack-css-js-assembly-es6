function HeaderController(HeaderFactory) {
    let vm = this;

    vm.logo = HeaderFactory.getLogo();
    vm.menuItems = HeaderFactory.getMenuItems();
}

HeaderController.$inject = ['HeaderFactory'];
export { HeaderController };