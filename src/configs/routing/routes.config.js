function RouteConfig($locationProvider, $stateProvider, $urlRouterProvider) {
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });

    $urlRouterProvider.otherwise("/404");

    $stateProvider
        .state('app', {
            url: '/',
            abstract: true,
            template: `<ui-view>`
        })
        .state('app.home', {
            url: '',
            templateUrl: '../../pages/home.html'
        })
        .state('app.category', {
            url: 'category',
            templateUrl: "../../pages/categories.html"
        })
}

RouteConfig.$inject = ['$locationProvider', '$stateProvider', '$urlRouterProvider'];
export {RouteConfig};